#pragma once
#include "image_format.h"
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize; 
    uint32_t biWidth; 
    uint32_t biHeight; 
    uint16_t biPlanes; 
    uint16_t biBitCount; 
    uint32_t biCompression; 
    uint32_t biSizeImage; 
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INVALID_DATA,
  READ_INVALID_PARAMETERS,
  READ_FILE_ERROR,
  READ_OUT_OF_MEMORY
  /* коды других ошибок  */
};

/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
  /* коды других ошибок  */
};

enum read_status from_bmp( FILE* file, struct image *image, struct bmp_header* header);
enum write_status to_bmp( FILE* out, const struct image *image, struct bmp_header const* header);
size_t get_padding(size_t width);
void report_bmp_read_error(enum read_status status);
void report_bmp_write_error(enum write_status status);
