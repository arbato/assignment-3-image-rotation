#pragma once
#include <stddef.h>
#include <stdint.h>

// TODO: Comments
struct pixel { uint8_t b, g, r; };

struct image {
    size_t width, height;
    struct pixel* data;
};

int create_image(struct image* image, size_t width, size_t height);
void delete_image(struct image* image);
