#include "image_format.h"
#include <stdio.h>
#include <stdlib.h>

int create_image(struct image* image, size_t width, size_t height) {
    image->width = width;
    image->height = height;
    image->data = malloc(width * height * sizeof(struct pixel));

    if (image->data == NULL) {
        return 1;
    }
    
    return 0;
}

void delete_image(struct image* image) {
    if (image == NULL) return;
    free(image->data);
}
