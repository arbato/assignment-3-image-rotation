#include <bmp_io.h>
#include <stdint.h>
#include <stdlib.h>


static uint16_t TYPE = 0x4D42;
static uint32_t RESERVED = 0;
static uint16_t BIT_COUNT = 24;
static uint16_t PLANE = 1;
static uint32_t BI_SIZE = 40;
static uint32_t COMPRESSION = 0;

static int bmp_header_valid(struct bmp_header const *header) {
    if (header->bfType != TYPE ||
        header->bfReserved != RESERVED ||
        header->bOffBits != sizeof(struct bmp_header) ||
        header->biSize != BI_SIZE ||
        header->biPlanes != PLANE ||
        header->biBitCount != BIT_COUNT ||
        header->biCompression != COMPRESSION ||
        header->biWidth < 1 ||
        header->biHeight < 1
    ) return 0;

    return 1;
}

enum read_status from_bmp(FILE* file, struct image *image, struct bmp_header* header) {
    if (file == NULL || image == NULL || header == NULL) return READ_INVALID_PARAMETERS;

    size_t len = fread(header, sizeof(struct bmp_header), 1, file);

    if (len != 1) return READ_FILE_ERROR;

    if (header->bfType != TYPE) return READ_INVALID_SIGNATURE;
    if (header->biBitCount != BIT_COUNT) return READ_INVALID_BITS;
    if (!bmp_header_valid(header)) return READ_INVALID_HEADER;

    if (fseek(file, (long) header->bOffBits, SEEK_SET) != 0) return READ_FILE_ERROR;

    int res = create_image(image, header->biWidth, header->biHeight);
    if (res) {
        return READ_OUT_OF_MEMORY;
    }

    size_t padding = get_padding(header->biWidth);

    for (size_t i = 0; i < header->biHeight; i++) {
        size_t temp = fread(image->data + header->biWidth * i, sizeof(struct pixel) * header->biWidth, 1, file);
        if (temp != 1) {
            delete_image(image);
            return READ_FILE_ERROR;
        }

        if (fseek(file, (long) padding, SEEK_CUR) != 0) {
            delete_image(image);
            return READ_FILE_ERROR;
        }
    }

    return READ_OK;
}

void report_bmp_read_error(enum read_status status) {
    const char* error_msg;
    switch (status) {
        case READ_INVALID_SIGNATURE:
            error_msg = "File is not bmp file.\n";
            break;
        case READ_INVALID_HEADER:
            error_msg = "File with incorrect header.\n";
            break;
        case READ_INVALID_BITS:
            error_msg = "Unsupported number of bits per pixel.\n";
            break;
        case READ_INVALID_DATA:
            error_msg = "Incorrect data in file.\n";
            break;
        case READ_FILE_ERROR:
            error_msg = "Error reading header from file.\n";
            break;
        case READ_OUT_OF_MEMORY:
            error_msg = "Error allocating memory.\n";
            break;    
        case READ_INVALID_PARAMETERS: 
            error_msg = "Incorrect parameters passed to from_bmp().\n";
            break;
        case READ_OK:
            return;
    }

    fprintf(stderr, "Error: %s", error_msg);
    exit(-1);
}


void report_bmp_write_error(enum write_status status) {
    const char* error_msg;
    switch (status) {
        case WRITE_ERROR:
            error_msg = "Error writing bmp to file.\n";
            break;
        case WRITE_OK:
            return;
    }

    fprintf(stderr, "Error: %s", error_msg);
    exit(-1);
}

enum write_status to_bmp(FILE* out, const struct image *image, struct bmp_header const* header) {
    if (!bmp_header_valid(header)) return WRITE_ERROR;
    struct bmp_header new_header = *header;

    new_header.bOffBits = sizeof(struct bmp_header);
    new_header.biWidth = image->width;
    new_header.biHeight = image->height;

    if (fwrite(&new_header, sizeof(struct bmp_header), 1, out) != 1) return WRITE_ERROR;
    size_t padding = get_padding(new_header.biWidth);
   
    for (size_t i = 0; i < new_header.biHeight; i++) {
        if (fwrite(image->data + new_header.biWidth * i, sizeof(struct pixel) * new_header.biWidth, 1, out) != 1) return WRITE_ERROR;
        if (padding) {
            size_t temp = 0;
            if (fwrite(&temp, padding, 1, out) != 1) return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}

size_t get_padding(size_t width) {
    if (width % 4 == 0) return 0;
    // else: (multiply by three because it is 3 pixels)
    return 4 - (width * 3) % 4 ;
}
