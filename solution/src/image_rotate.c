#include <image_format.h>
#include <image_rotate.h>
#include <math.h>
#include <stdio.h>

int image_rotate(struct image* image, long angle) {    
    int sx;
    int sy;
    size_t offset;
    size_t width = image->width;
    size_t height = image->height;

    angle = (angle % 360 + 360) % 360;
    switch (angle) {
        case 0:
            sx = 1;
            sy = (int) width;
            offset = 0;
            break;
        case 90:
            sx = - (int) height;
            sy = 1;
            offset = width * height - height;
            width = height;
            height = image->width;
            break;
        case 180:
            sx = -1;
            sy = - (int) width;
            offset = width * height - 1;
            break;
        case 270:
            sx = (int) height;
            sy = -1;
            offset =  height - 1;
            width = height;
            height = image->width;
            break;
        default:
            return 1;
    }
    struct image result;
    int r = create_image(&result, width, height);
    if (r) return 1; // 1 means error
    for (size_t y = 0; y < image->height; y++) {
        for (size_t x = 0; x < image->width; x++) {
            result.data[x * sx + y * sy + offset] = image->data[x + y * image->width];
        }
    }
    delete_image(image);
    *image = result;
    return 0;
}


