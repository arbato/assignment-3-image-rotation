#include "file_io.h"
#include <stdio.h>
#include <stdlib.h>


void fopen_read(const char* file_name, FILE** input){
    FILE* res = fopen(file_name, "rb");
    if (res == NULL) {
        fprintf(stderr, "Failed to open file for reading: %s\n", file_name);
        exit(-1);
    }
    *input = res;
}

void fopen_write(const char* file_name, FILE** output){
    FILE* res = fopen(file_name, "wb");
    if (res == NULL) {
        fprintf(stderr, "Failed to open file for writing: %s\n", file_name);
        exit(-1);
    }
    *output = res;
}

void close(FILE** input){
    int res = fclose(*input);
    if (res != 0) {
        fprintf(stderr, "Failed to close file.\n");
        exit(-1);
    }
}
