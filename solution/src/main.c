#include <bmp_io.h>
#include <file_io.h>
#include <image_rotate.h>
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>


int main( int argc, char** argv) {
    if (argc !=4){
        fprintf(stderr, "Incorrect number of args, expected: <input-image> <rotated-image> <angle>");
        return -1;
    }

    long angle = strtol((argv[3]), (char **)'\0', 10);
    if(angle % 90 != 0){
        fprintf(stderr, "Invalid angle, only multiples of 90 supported.");
        return -1;
    }    

    if(angle > 270 || angle < -270){
        fprintf(stderr, "Invalid angle, must be between -270 and 270");
        return -1;
    }

    FILE* input;
    fopen_read(argv[1], &input);

    struct image our_image;
    struct bmp_header header; 

    memset(&our_image, 0, sizeof(our_image));
    enum read_status r_status = from_bmp(input, &our_image, &header);
    close(&input);

    if (r_status != READ_OK) {
        report_bmp_read_error(r_status);
    }

    int res = image_rotate(&our_image, angle);
    if (res) {
        fprintf(stderr, "image rotation failed");
        return -1;
    }

    FILE* output;
    fopen_write(argv[2], &output);
    enum write_status w_status = to_bmp(output, &our_image, &header);
    close(&output);

    delete_image(&our_image);

    if (w_status != WRITE_OK) {
        report_bmp_write_error(w_status);
    }

    return 0;
}




